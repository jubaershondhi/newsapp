import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:news_app_api_asn/views/details_view.dart';
import 'package:news_app_api_asn/views/home_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUpPage extends StatefulWidget {

  @override
  _SignUpWidgetState createState() => _SignUpWidgetState();
}

class _SignUpWidgetState extends State<SignUpPage>{


  final emailControllerSignUp = TextEditingController();
  final passwordControllerSignUp = TextEditingController();

  @override
  void dispose() {
    emailControllerSignUp.dispose();
    passwordControllerSignUp.dispose();

    super.dispose();
  }


  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.cyan,
      title: Text("News App"),
    ),
    body: _signUpWidget(),
  );


  Widget _signUpWidget(){
    return Container(
      margin: EdgeInsets.all(10),
      child: Column(
        children: [
          TextFormField(
            controller: emailControllerSignUp,
            validator: (value) {
            },
            style: TextStyle(color: Colors.black),
            onChanged: (v) {
              //_controller.userExists = "unique".obs;
              /*_controller.updateButtonStatus();*/
            },
            decoration: const InputDecoration(
              contentPadding: const EdgeInsets.symmetric(
                  vertical: 10.0, horizontal: 10.0),
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide:
                const BorderSide(color: Color(0xFF9A9A9A), width: 1.0),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide: BorderSide(color: Color(0xFF9A9A9A), width: 2.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide: BorderSide(color: Color(0xFF9A9A9A), width: 2.0),
              ),
              fillColor: Colors.black,
              label: const Text('Mobile',
                  style: TextStyle(color: Color(0xFF9A9A9A))),
              labelStyle: TextStyle(color: Colors.black),
            ),
          ),
          SizedBox(height: 15),
          TextFormField(
            controller: passwordControllerSignUp,
            validator: (value) {
            },
            style: TextStyle(color: Colors.black),
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(
                  vertical: 10.0, horizontal: 10.0),
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide:
                const BorderSide(color: Color(0xFF9A9A9A), width: 1.0),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide: BorderSide(color: Color(0xFF9A9A9A), width: 2.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide: BorderSide(color: Color(0xFF9A9A9A), width: 2.0),
              ),
              label: const Text('Password',
                  style: TextStyle(color: Color(0xFF9A9A9A))),
              labelStyle: TextStyle(color: Colors.black),
            ),
          ),
          SizedBox(height: 15),
          Container(
            child: Center(
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(8), // <-- Radius
                          ),
                          primary: Color(0xFF2F80EC),
                        ),
                        onPressed: () async {
                          signUp();
                          Get.off(HomePage());
                        },
                        child: Container(
                            margin: EdgeInsets.fromLTRB(0, 12, 0, 12),
                            child: const Text(
                              "Sign Up",
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }


  Future signUp() async{
    await FirebaseAuth.instance.createUserWithEmailAndPassword(email: emailControllerSignUp.text, password: passwordControllerSignUp.text);
  }
}
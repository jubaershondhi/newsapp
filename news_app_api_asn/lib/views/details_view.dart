
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bookmark_view.dart';

class DetailsPage extends StatefulWidget {
  final String title;
  final String img;
  final String author;
  final String content;
  final String sourceName;
  final String date;

  DetailsPage(this.title, this.author,this.img, this.content,this.sourceName, this.date);

  @override
  _DetailsWidgetState createState() => _DetailsWidgetState(title, author, img, content, sourceName, date);
}

class _DetailsWidgetState extends State<DetailsPage>{

  final String title;
  final String img;
  final String author;
  final String content;
  final String sourceName;
  final String date;

  _DetailsWidgetState(this.title, this.author,this.img, this.content,this.sourceName, this.date);

  String userUniqueEmail = "";

  @override
  initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      userEmailCheck();
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.cyan,
        title: Text("News App"),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10.0),
        child:Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Title: ' + title,
              style: new TextStyle(
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10,),
            Text(
              'Author: ' + author,
              style: new TextStyle(
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10,),
            Text(
              'Date: ' + date,
              style: new TextStyle(
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10,),
            Text(
              'Source: ' + sourceName,
              style: new TextStyle(
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10,),
            ElevatedButton.icon(
                onPressed: () async {
                  Map <String,dynamic> data = {"title":title, "author":author, "img":img, "content":content, "sourceName":sourceName, "date":date, "uniqueEmail":userUniqueEmail};
                  FirebaseFirestore.instance.collection(userUniqueEmail).add(data);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => BookMarkPage()));




                },
                icon: Icon(Icons.bookmark),
                style: ElevatedButton.styleFrom(
                  primary: Colors.cyan,
                ),
                label: Text("Bookmark")),
            SizedBox(height: 10,),
            Image.network(
              img,
              fit: BoxFit.fill,
              errorBuilder: (BuildContext context,
                  Object exception,
                  StackTrace? stackTrace) {
                return const Text(
                    'Image is not available!');
              },
            ),
            SizedBox(height: 10,),
            Text(
              'Description: ' + content,
              style: new TextStyle(
                  fontWeight: FontWeight.bold),
            ),


          ],

        ),
      ),

    );
  }

  Future<void> userEmailCheck() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    userUniqueEmail = await prefs.getString("user_unique_email")??'';
    var a;
  }

}



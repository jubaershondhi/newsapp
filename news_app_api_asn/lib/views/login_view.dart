import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:news_app_api_asn/views/details_view.dart';
import 'package:news_app_api_asn/views/home_view.dart';
import 'package:news_app_api_asn/views/signup_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  final String title;
  final String img;
  final String author;
  final String content;
  final String sourceName;
  final String date;

  LoginPage(this.title, this.author,this.img, this.content,this.sourceName, this.date);

  @override
  _LoginWidgetState createState() => _LoginWidgetState(title, author, img, content, sourceName, date);
}

class _LoginWidgetState extends State<LoginPage>{

  final String title;
  final String img;
  final String author;
  final String content;
  final String sourceName;
  final String date;

  _LoginWidgetState(this.title, this.author,this.img, this.content,this.sourceName, this.date);

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();

    super.dispose();
  }


  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      backgroundColor: Colors.cyan,
      title: Text("News App"),
    ),
    body: StreamBuilder<User?>(
      stream: FirebaseAuth.instance.authStateChanges(),
      builder: (context, snapshot) {
        if(snapshot.hasData){
          return DetailsPage(title, author, img, content, sourceName, date);
        }
        else{
          return _loginWidget();
        }

      },
    ),

  );

  Widget _loginWidget(){
    return Container(
      margin: EdgeInsets.all(10),
      child: Column(
        children: [
          TextFormField(
            controller: emailController,
            validator: (value) {
            },
            style: TextStyle(color: Colors.black),
            onChanged: (v) {
              //_controller.userExists = "unique".obs;
              /*_controller.updateButtonStatus();*/
            },
            decoration: const InputDecoration(
              contentPadding: const EdgeInsets.symmetric(
                  vertical: 10.0, horizontal: 10.0),
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide:
                const BorderSide(color: Color(0xFF9A9A9A), width: 1.0),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide: BorderSide(color: Color(0xFF9A9A9A), width: 2.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide: BorderSide(color: Color(0xFF9A9A9A), width: 2.0),
              ),
              fillColor: Colors.black,
              label: const Text('Mobile',
                  style: TextStyle(color: Color(0xFF9A9A9A))),
              labelStyle: TextStyle(color: Colors.black),
            ),
          ),
          SizedBox(height: 15),
          TextFormField(
            controller: passwordController,
            validator: (value) {
            },
            style: TextStyle(color: Colors.black),
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(
                  vertical: 10.0, horizontal: 10.0),
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide:
                const BorderSide(color: Color(0xFF9A9A9A), width: 1.0),
              ),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide: BorderSide(color: Color(0xFF9A9A9A), width: 2.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                borderSide: BorderSide(color: Color(0xFF9A9A9A), width: 2.0),
              ),
              label: const Text('Password',
                  style: TextStyle(color: Color(0xFF9A9A9A))),
              labelStyle: TextStyle(color: Colors.black),
            ),
          ),
          SizedBox(height: 15),
          Container(
            child: Center(
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(8), // <-- Radius
                          ),
                          primary: Color(0xFF2F80EC),
                        ),
                        onPressed: () async {
                                signIn();
                             },
                        child: Container(
                            margin: EdgeInsets.fromLTRB(0, 12, 0, 12),
                            child: const Text(
                              "Login",
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 15),
          Container(
            child: Center(
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius:
                            BorderRadius.circular(8), // <-- Radius
                          ),
                          primary: Color(0xFF2F80EC),
                        ),
                        onPressed: () async {
                          Get.off(SignUpPage());
                        },
                        child: Container(
                            margin: EdgeInsets.fromLTRB(0, 12, 0, 12),
                            child: const Text(
                              "Sign Up",
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }



  Future signIn() async {
    await FirebaseAuth.instance.signInWithEmailAndPassword(email: emailController.text, password: passwordController.text);
    String userUniqueEmail = emailController.text;
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("user_unique_email", userUniqueEmail);
    var a;
  }
}
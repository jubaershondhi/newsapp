import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:http/http.dart';
import 'package:news_app_api_asn/controllers/news_controller.dart';
import 'package:news_app_api_asn/views/login_view.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bookmark_view.dart';
import 'details_view.dart';


class HomePage extends StatefulWidget {

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomePage>{

  final NewsController newsController = Get.put(NewsController());

  String userUniqueEmail = "";

  @override
  initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      userEmailCheck();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.cyan,
        title: Text("News App"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.white,
            ),
            onPressed: () {
              PopMenu(context);
            },
          )
        ],
      ),
      body: Obx(() => newsController.isLoading.value
          ? Center(
        child: CircularProgressIndicator(),
      )
          : ListView.builder(
          itemCount: newsController.articles.length,
          physics: BouncingScrollPhysics(),
          itemBuilder: (context, index) {
            String title = newsController.articles[index].title ?? '';
            String author = newsController.articles[index].author ?? '';
            String content = newsController.articles[index].content ?? '';
            String? img2 = newsController.articles[index].urlToImage;
            String? date2 = newsController.articles[index].publishedAt??'';
            var formattedDate = DateTime.parse(date2);
            var date =
                "${formattedDate.day}-${formattedDate.month}-${formattedDate.year}";
            String img = "";
            String description =
                newsController.articles[index].description ?? '';
            String sourceName =
            newsController.articles[index].source!.name!;
            var a;
            if (img2 != null) {
              img = img2;
            } else {
              img =
              "https://www.haliburtonforest.com/wp-content/uploads/2017/08/placeholder-square.jpg";
            }

            return Container(
              margin: EdgeInsets.all(5),
              child: Column(
                children: [
                  InkWell(
                    onTap: () {
                      if(userUniqueEmail==""){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage(title, author, img, content, sourceName, date)));

                      }
                      else{
                           Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DetailsPage(title, author, img, content, sourceName, date)));
                      }


                    },
                    child: Card(
                      elevation: 10,
                      child: Padding(
                        padding: EdgeInsets.all(5),
                        child: Column(
                          children: [
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  'Title: ' + title,
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold),
                                )),
                            SizedBox(height: 5),
                            Image.network(
                              img,
                              fit: BoxFit.fill,
                              errorBuilder: (BuildContext context,
                                  Object exception,
                                  StackTrace? stackTrace) {
                                return const Text(
                                    'Image is not available!');
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            );
          })),
    );
  }

  void PopMenu(context) {
    showMenu<String>(
      context: context,
      position: RelativeRect.fromLTRB(100.0, 110.0, 40.0, 0.0),
      //position where you want to show the menu on screen
      items: [
        if(userUniqueEmail=="")...{
          PopupMenuItem<String>(
              child: const Text('Login'), value: '1'),

        }
        else...{
          PopupMenuItem<String>(
              child: const Text('Login'), value: '1'),

          PopupMenuItem<String>(child: const Text('Bookmark List'), value: '2'),
          PopupMenuItem<String>(child: const Text('Logout'), value: '3'),
        }

      ],
      elevation: 8.0,
    ).then<void>((itemSelected) async {
      if (itemSelected == "1") {
        ScaffoldMessenger.of(context)
            .showSnackBar(
          const SnackBar(
              content: Text(
                  "You have to login by clicking on News Details")),
        );
      } else if (itemSelected == "2") {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => BookMarkPage()));
      } else if (itemSelected == "3") {
        FirebaseAuth.instance.signOut();
        final prefs = await SharedPreferences.getInstance();
        prefs.clear();
        Get.offAll(HomePage());

      }
    });
  }

  Future<void> userEmailCheck() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    userUniqueEmail = await prefs.getString("user_unique_email")??'';
    var a;
    setState(() {
      userUniqueEmail = userUniqueEmail;
    });
  }

}



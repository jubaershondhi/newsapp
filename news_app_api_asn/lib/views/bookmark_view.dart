import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:news_app_api_asn/views/home_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BookMarkPage extends StatefulWidget {
  @override
  _BookMarkWidgetState createState() => _BookMarkWidgetState();
}

class _BookMarkWidgetState extends State<BookMarkPage>{


  String userUniqueEmail = "";

  @override
  initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      userEmailCheck();
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) => Scaffold(
    backgroundColor: Colors.white,
    appBar: AppBar(
      backgroundColor: Colors.cyan,
      title: Text("News App"),
    ),
    body: StreamBuilder(
      stream: FirebaseFirestore.instance.collection(userUniqueEmail).snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
        if(!snapshot.hasData){
          return Text("No data");
        }
        return ListView(
          children: (snapshot.data! as QuerySnapshot).docs.map((document){
            String title = document["title"]??'';
            String author = document["author"]??'';
            String sourceName = document["sourceName"]??'';
            //String? img2 = newsController.articles[index].urlToImage;
            String date = document["date"]??'';
            String img = document["img"]??'';
            String content = document["content"]??'';
            var a;
              return Container(
                margin: EdgeInsets.all(5),
                child: Card(
                  elevation: 10,
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Title: ' + title,
                          style: new TextStyle(
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          'Author: ' + author,
                          style: new TextStyle(
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          'Date: ' + date,
                          style: new TextStyle(
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10,),
                        Text(
                          'Source: ' + sourceName,
                          style: new TextStyle(
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10,),
                        Image.network(
                          img,
                          fit: BoxFit.fill,
                          errorBuilder: (BuildContext context,
                              Object exception,
                              StackTrace? stackTrace) {
                            return const Text(
                                'Image is not available!');
                          },
                        ),
                        SizedBox(height: 10,),
                        Text(
                          'Description: ' + content,
                          style: new TextStyle(
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              );
          },
        ).toList(),
        );
      },
    ),

  );



  Future<void> userEmailCheck() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    userUniqueEmail = await prefs.getString("user_unique_email")??'';

    setState(() {
      userUniqueEmail = userUniqueEmail;
    });
    var a;
  }



}
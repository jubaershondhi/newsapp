import 'package:get/get.dart';
import 'package:news_app_api_asn/service/news_service.dart';

import '../models/article_model.dart';

class NewsController extends GetxController {
  var articles = <Articles>[].obs;
  var isLoading = true.obs;

  @override
  void onInit(){
    getArticles();
    super.onInit();
  }

  void getArticles() async {
    try {
      isLoading(true);
      var articlesTemp = await NewsWebService.getNews();
      if(articlesTemp != null){
        articles(articlesTemp);
      }
    } finally {
      isLoading(false);
    }
  }
}

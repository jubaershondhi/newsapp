import 'package:http/http.dart' as http;
import 'package:news_app_api_asn/constants/constants.dart';

import '../models/article_model.dart';

class NewsWebService {

  static var client = http.Client();

  static Future<List<Articles>?> getNews() async {

    var response = await client.get(Uri.parse(Constants.TOPHEADLINES));

    if(response.statusCode == 200){
      return articlesFromJson(response.body);
    }
    else
      {
        return null;
      }

  }

}
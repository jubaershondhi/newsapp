**NewsApp Application**

NewsApp is an Application, that shows the trending news in a listview. I use newsApi and also integrate login and signup process. There is also work of shared preference. I use getx in the project. If any user want to login from other mobile the user can easily access his data. User can bookmark the news and can see it in a list.

Used technologies:
Framework: Flutter.
<br>Language: Dart.
<br>IDE: Android studio.

Used Libraries:

dependencies:
  flutter:
    sdk: flutter


  # The following adds the Cupertino Icons font to your application.
  # Use with the CupertinoIcons class for iOS style icons.
  cupertino_icons: ^1.0.2
  get: ^4.5.1
  http: ^0.13.4
  cloud_firestore: ^4.3.1
  firebase_auth: ^4.2.5
  firebase_core: ^2.4.1
  shared_preferences: ^2.0.11

  Screenshots Link: https://drive.google.com/drive/folders/19ZV32lCoQtQFjhtlE4z8p34G2pGACn9g?usp=sharing 
  <br>Apk Link: https://drive.google.com/drive/folders/1eVpjgB61uzEEizPn6tzSnQQrdkYxWUsr?usp=sharing 
